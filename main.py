from flask import Flask, request
from config import token
import sqlite3
import requests

app = Flask(__name__)

url = 'https://api.telegram.org/bot{0}/sendMessage'.format(token)

conn = sqlite3.connect('db.sqlite3', check_same_thread=False)
curs = conn.cursor()

alphabet = '0123456789' \
           ' ~`!@"#№$;%^:&?*()_-+=[]{}|\/\'<>.,' \
           'абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ' \
           'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'


def get_id(json):
    return json['message']['chat']['id']


def get_text(json):
    return json['message']['text']


def add_user(chat_id):
    user = curs.execute("SELECT chat_id FROM users WHERE chat_id={0}".format(chat_id)).fetchone()
    if user is None:
        curs.execute(
            "INSERT INTO users VALUES ({0}, {1}, {2}, {3}, {4})".format(chat_id, 0, False, False, False))
        conn.commit()


def send_mess(chat_id, mess, names_btn=[]):
    if len(names_btn) == 0:
        response = {'chat_id': chat_id, 'text': mess}
        requests.post(url=url, json=response)
    elif len(names_btn) != 0:
        buttons = [[{'text': names_btn[0]}],
                   [{'text': names_btn[1]}],
                   [{'text': names_btn[2]}]]
        response = {'chat_id': chat_id, 'text': mess,
                    'reply_markup': {'keyboard': buttons, 'resize_keyboard': True}}
        requests.post(url=url, json=response)


@app.route('/', methods=['POST', 'GET'])
def text():
    if request.method == "POST":
        json = request.get_json()
        chat_id = get_id(json)
        txt = get_text(json)

        # Команда /start
        if txt == '/start':
            send_mess(chat_id=chat_id,
                      mess='Привет {0} ты запустил CryptoBot\n\n'
                           'Я могу зашифровывать слова и расшифрововать их.\n'
                           'Удачи в использовании!!! ☺\n\n'
                           'Помни, если будешь вводить цифровой ключ, то набирай максимум 18 цифр'.format(
                          json['message']['chat']['first_name']),
                      names_btn=['зашифровывать', 'поменять ключ', 'расшифровывать'])
            add_user(chat_id)

        # Перевод бота в режим зашифровки
        elif txt == 'зашифровывать':
            send_mess(chat_id=chat_id, mess='Бот переведен в режим зашифровки')
            curs.execute(
                "UPDATE users SET m1={0}, m2={1}, m3={2} WHERE chat_id={3}".format(False, True, False, chat_id))
            conn.commit()

        # Поменять ключ
        elif txt == 'поменять ключ':
            send_mess(chat_id=chat_id, mess='Введите цифровой ключ')
            curs.execute("UPDATE users SET m1={0} WHERE chat_id={1}".format(True, chat_id))
            conn.commit()

        # Перевод бота в режим расшифровки
        elif txt == 'расшифровывать':
            send_mess(chat_id=chat_id, mess='Бот переведен в режим расшифровки')
            curs.execute(
                "UPDATE users SET m1={0}, m2={1}, m3={2} WHERE chat_id={3}".format(False, False, True, chat_id))
            conn.commit()

        # Прием введеного текста
        elif True:
            m1, m2, m3, key = curs.execute(
                "SELECT m1, m2, m3, key FROM users WHERE chat_id={0}".format(chat_id)).fetchone()

            if m1:
                try:
                    curs.execute(
                        "UPDATE users SET m1={0}, key={1} WHERE chat_id={2}".format(False, int(txt),
                                                                                    chat_id))
                    conn.commit()
                except ValueError:
                    curs.execute(
                        "UPDATE users SET m1={0} WHERE chat_id={1}".format(False, chat_id))
                    conn.commit()
                    return send_mess(chat_id, 'Error')
                curs.execute(
                    "UPDATE users SET m1={0} WHERE chat_id={1}".format(False, chat_id))
                conn.commit()
                send_mess(chat_id, 'OK')

            elif m2:
                out = ''
                try:
                    try:
                        for j in txt:
                            out += alphabet[(alphabet.index(j) + key) % (alphabet.index(alphabet[-1]) + 1)]
                    except ValueError:
                        return send_mess(chat_id, 'К сожалению у меня не получается закодировать эту строку')
                except TypeError:
                    return send_mess(chat_id, 'Error')
                send_mess(chat_id, out)

            elif m3:
                out = ''
                try:
                    try:
                        for j in txt:
                            out += alphabet[(alphabet.index(j) - key) % (alphabet.index(alphabet[-1]) + 1)]
                    except ValueError:
                        return send_mess(chat_id, 'К сожалению у меня не получается раскодировать эту строку')
                except TypeError:
                    return send_mess(chat_id, 'Error')
                send_mess(chat_id, out)

    return {"ok": True}


app.run(host='192.168.1.106', port=5000)
